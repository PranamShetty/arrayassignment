function reduce(ele, cb, startingValue) {
    if (!Array.isArray(ele) || !cb) {
      return [];
    }
    if (startingValue === undefined) {
      startingValue = ele[0];
    }
    else startingValue = cb(startingValue,ele[0],ele)   
    for (let index = 1; index < ele.length; index++) {
      let items = ele[index];
      startingValue = cb(startingValue, items,index, ele);
    }
    return startingValue;
  };


module.exports = reduce 
   
    




