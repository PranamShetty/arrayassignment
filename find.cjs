function find(element,cb){
    if(!(Array.isArray(element) || !cb)){
        return [];
} 

let result = []
for(let index= 0; index < element.length; index++ ){
    let ans = cb(element[index])
    if(ans){
        result.push(ans)
        break;
    }
}
return result
}

module.exports = find