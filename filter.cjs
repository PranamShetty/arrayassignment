function filter(elements, cb) {
    if (!Array.isArray(elements) || !cb) {
      return [];
    }
    let filterResult = [];
    for (let index = 0; index < elements.length; index++) {
      if (cb(elements[index], index, elements)==true) {
        filterResult.push(elements[index]);
      }
    }
    return filterResult;
  };
  

module.exports = filter