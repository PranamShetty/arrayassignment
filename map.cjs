function map(elements, cb) {
    if (!Array.isArray(elements) || !cb) {
      return [];
    } else {
      let answer = [];
      for (let index = 0; index < elements.length; index++) {
        answer.push(cb(elements[index], index, elements));
      }
      return answer;
    }
  };

  module.exports = map
  