function flatten(elements, depth) {
  if (!Array.isArray(elements)) {
      return [];
    }
if (depth === undefined) {
  depth = 1;
}
let result = [];
for (let index = 0; index < elements.length; index++) {
  if (Array.isArray(elements[index]) && depth > 0) {
    result = result.concat(flatten(elements[index], depth - 1));
  } else if (elements[index] === undefined) continue;
  else {
    result.push(elements[index]);
  }
}
return result;
};


module.exports = flatten
